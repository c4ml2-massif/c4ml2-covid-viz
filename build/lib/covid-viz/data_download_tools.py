import requests
import pandas as pd
import numpy as np
import csv
import glob

### URL Variables
NEW_DAILY_CASE_VAX_DATAURL = 'https://api.coronavirus.data.gov.uk/v2/data?areaType=ltla&metric=newCasesByPublishDate&metric=newPeopleVaccinatedFirstDoseByVaccinationDate&metric=newPeopleVaccinatedSecondDoseByVaccinationDate&metric=newPeopleVaccinatedThirdInjectionByVaccinationDate&format=csv'
ENG_NEW_DAILY_CASE_VAX_DATAURL = 'https://api.coronavirus.data.gov.uk/v2/data?areaType=nation&metric=newCasesByPublishDate&metric=newPeopleVaccinatedFirstDoseByVaccinationDate&metric=newPeopleVaccinatedSecondDoseByVaccinationDate&metric=newPeopleVaccinatedThirdInjectionByVaccinationDate&format=csv'

CUM_DAILY_DATAURL = 'https://api.coronavirus.data.gov.uk/v2/data?areaType=ltla&metric=cumCasesByPublishDate&metric=cumPeopleVaccinatedFirstDoseByVaccinationDate&metric=cumPeopleVaccinatedSecondDoseByVaccinationDate&metric=cumPeopleVaccinatedThirdInjectionByVaccinationDate&metric=cumCasesByPublishDateRate&format=csv'
ENG_CUM_DAILY_DATAURL = 'https://api.coronavirus.data.gov.uk/v2/data?areaType=nation&metric=cumCasesByPublishDate&metric=cumPeopleVaccinatedFirstDoseByVaccinationDate&metric=cumPeopleVaccinatedSecondDoseByVaccinationDate&metric=cumPeopleVaccinatedThirdInjectionByVaccinationDate&metric=cumCasesByPublishDateRate&format=csv'

def return_gov_data(URL):
    '''
    Helper function to download coronavirus data.

    Params:
    ------
    URL: str
        Permanent link provided from https://coronavirus.data.gov.uk/details/download to retrieve English Covid related data.

    Returns:
    -------
    pd.Dataframe
        Raw dataset as requested.
    '''
    response = requests.get(URL)

    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        # For all status  codes except 200 - i.e. problem
        return "Error: " + str(e)

    # 200 status code - i.e. working
    data = pd.read_csv(URL)
    return data


#############################################################################################################
def rate_div(name):
    '''
    This function checks if the lower tier local authroity area provided
    corresponds to an area in the gov.uk dataset that is the result of a merge
    (see example below). Areas that are the result of a merge are assigned a
    divisor that will be used to adjust the COVID-19 case rate per 100,000
    individuals. The function is compatible with the pd.DataFrame.apply()
    method and so takes a single input element. Example merge: 'East Suffolk'
    is the merge of 'Suffolk Coastal' and 'Waveney' hence it requires a divisor
    of 2.

    Parameters
    ----------
    name : str
        The name of a lower tier local authority.

    Returns
    -------
    int

    '''
    # store all lower tier local authority areas that we produced through the
    # transform_code merge
    divs_dict = {'Bournemouth, Christchurch and Poole': 3,
                 'Buckinghamshire': 4}
    divs_keys = list(divs_dict.keys())
    # intitialise cases rate divisor for areas that are not a merge of smaller
    # areas
    div = 1
    if divs_keys.count(name) > 0:
        # use divs_dict to assign the correct divisor for each area name in
        # divs_dict
        div = divs_dict[name]
    return (div)


################################################################################################################
def transform_code(item_in, out='code', code_dict={'E06000028': 'E06000058',
                                                   'E06000029': 'E06000058',
                                                   'E07000048': 'E06000058',
                                                   'E07000049': 'E06000059',
                                                   'E07000050': 'E06000059',
                                                   'E07000051': 'E06000059',
                                                   'E07000050': 'E06000059',
                                                   'E07000052': 'E06000059',
                                                   'E07000053': 'E06000059',
                                                   'E07000097': 'E07000242',
                                                   'E08000020': 'E08000037',
                                                   'E06000048': 'E06000057',
                                                   'E07000100': 'E07000240',
                                                   'E07000101': 'E07000243',
                                                   'E07000104': 'E07000241',
                                                   'E07000191': 'E07000246',
                                                   'E07000190': 'E07000246',
                                                   'E07000004': 'E06000060',
                                                   'E07000005': 'E06000060',
                                                   'E07000006': 'E06000060',
                                                   'E07000007': 'E06000060',
                                                   'E07000205': 'E07000244',
                                                   'E07000206': 'E07000244',
                                                   'E07000201': 'E07000245',
                                                   'E07000204': 'E07000245',
                                                   'E09000033': 'E41000324',
                                                   'E06000052': 'E41000052'},
                   name_dict={'Bournemouth':
                                  'Bournemouth, Christchurch and Poole',
                              'Christchurch':
                                  'Bournemouth, Christchurch and Poole',
                              'Poole':
                                  'Bournemouth, Christchurch and Poole',
                              'East Dorset': 'Dorset',
                              'North Dorset': 'Dorset',
                              'Purbeck': 'Dorset',
                              'West Dorset': 'Dorset',
                              'Weymouth and Portland': 'Dorset',
                              'West Somerset': 'Somerset West and Taunton',
                              'Taunton Deane': 'Somerset West and Taunton',
                              'Aylesbury Vale': 'Buckinghamshire',
                              'Chiltern': 'Buckinghamshire',
                              'South Bucks': 'Buckinghamshire',
                              'Wycombe': 'Buckinghamshire',
                              'Suffolk Coastal': 'East Suffolk',
                              'Waveney': 'East Suffolk',
                              'Forest Heath': 'West Suffolk',
                              'St Edmundsbury': 'West Suffolk',
                              'Westminster': 'City of London,Westminster',
                              'Cornwall and Isles of Scilly':
                                  'Cornwall,Isles of Scilly',
                              'Shepway': 'Folkestone and Hythe',
                              'Hackney and City of London': 'Hackney'}):
    '''
    This function takes either area code or area name from the geoJSON or
    gov.uk datasets for lower local tier authorities. If the code or name is
    not consistent across the geoJSON, gov.uk, and ONS source data, it is
    updated.

    Parameters
    ----------
    item_in :  string
        A single string containing either an area code or area name.
    out :  string, optional
        A string specifying whether a code (out='code') or area (out='area')
        update is needed. The default is 'code'
    code_dict :  dictionary, optional
        A dictionary that updates area codes which are not present in all
        three data sources. The default is {'E06000028': 'E06000058',
                                            'E06000029': 'E06000058',
                                            'E07000048': 'E06000058',
                                            'E07000049': 'E06000059',
                                            'E07000050': 'E06000059',
                                            'E07000051': 'E06000059',
                                            'E07000050': 'E06000059',
                                            'E07000052': 'E06000059',
                                            'E07000053': 'E06000059',
                                            'E07000097': 'E07000242',
                                            'E08000020': 'E08000037',
                                            'E06000048': 'E06000057',
                                            'E07000100': 'E07000240',
                                            'E07000101': 'E07000243',
                                            'E07000104': 'E07000241',
                                            'E07000191': 'E07000246',
                                            'E07000190': 'E07000246',
                                            'E07000004': 'E06000060',
                                            'E07000005': 'E06000060',
                                            'E07000006': 'E06000060',
                                            'E07000007': 'E06000060',
                                            'E07000205': 'E07000244',
                                            'E07000206': 'E07000244',
                                            'E07000201': 'E07000245',
                                            'E07000204': 'E07000245',
                                            'E09000033': 'E41000324',
                                            'E06000052': 'E41000052'}.
    name_dict :  dictionary, optional
        A dictionary that updates area names which are not present in all
        three data sources.
        The default is {'Bournemouth': 'Bournemouth, Christchurch and Poole',
                        'Christchurch': 'Bournemouth, Christchurch and Poole',
                        'Poole': 'Bournemouth, Christchurch and Poole',
                        'East Dorset': 'Dorset',
                        'North Dorset': 'Dorset',
                        'Purbeck': 'Dorset',
                        'West Dorset': 'Dorset',
                        'Weymouth and Portland': 'Dorset',
                        'West Somerset': 'Somerset West and Taunton',
                        'Taunton Deane': 'Somerset West and Taunton',
                        'Aylesbury Vale': 'Buckinghamshire',
                        'Chiltern': 'Buckinghamshire',
                        'South Bucks': 'Buckinghamshire',
                        'Wycombe': 'Buckinghamshire',
                        'Suffolk Coastal': 'East Suffolk',
                        'Waveney': 'East Suffolk',
                        'Forest Heath': 'West Suffolk',
                        'St Edmundsbury': 'West Suffolk',
                        'Westminster': 'City of London,Westminster',
                        'Cornwall and Isles of Scilly':
                        'Cornwall,Isles of Scilly',
                        'Shepway': 'Folkestone and Hythe',
                        'Hackney and City of London': 'Hackney'}}.

    Returns
    -------
    string

    '''
    # create a list of the codes that need to be updated
    old_codes = list(code_dict.keys())
    # determine if codes or names are being checked
    if out == 'code':
        if old_codes.count(item_in) == 1:
            # if the code is present in code_dict.keys() then it needs to be
            # updated
            new_code = code_dict[item_in]
        else:
            # else the input value is output
            new_code = item_in
        return (new_code)
    # determine if names are being checked instead of codes
    elif out == 'name':
        # create a list of the names that need to be updated
        names_to_update = list(name_dict.keys())
        if names_to_update.count(item_in) == 1:
            # the name is not consistent across all three data sources and so
            # is changed
            new_name = name_dict[item_in]
        else:
            # else simply return the original name
            new_name = item_in
        return (new_name)
    else:
        # dictionaries for only area codes and names have been set up with
        # this function
        return ('Error: Input argument out must equal either "code" or "name"')


################################################################################################################
def clean_create_7day_rolling_average(input_df, output_file_name):
    '''
    Helper function to clean the netherlands covid dataset.

    Params:
    ------
    df: pd.Dataframe
        Raw dataset as requested (in dataframe).

    Returns:
    -------
    pd.Dataframe
        Cleaned 7day rolling average dataframe.

    '''
    clean_column_names = {'code_clean': 'code',
                          'area_clean': 'name',
                          'date_dt': 'Date',
                          'newCasesByPublishDate' : 'New Cases by Date',
                          'newCasesByPublishDate_7day_rolling': 'New Cases (7 Day Avg)',
                          'newPeopleVaccinatedFirstDoseByVaccinationDate_7day_rolling': 'Dose 1 (7 Day Avg)',
                          'newPeopleVaccinatedSecondDoseByVaccinationDate_7day_rolling': 'Dose 2 (7 Day Avg)',
                          'newPeopleVaccinatedThirdInjectionByVaccinationDate_7day_rolling': 'Dose 3 (7 Day Avg)'}

    new_column_names = []

    df = input_df.copy()
    df['date_dt'] = pd.to_datetime(df["date"], format='%Y-%m-%d', errors='ignore')
    df['code_clean'] = df['areaCode'].apply(transform_code)
    df['area_clean'] = df['areaName'].apply(lambda x: transform_code(x, out='name'))
    df['country'] = df['code_clean'].astype(str).str[0]
    df = df[df['country'] == 'E']
    df = df.groupby(['code_clean', 'area_clean', 'date_dt']).sum()
    df = df.reset_index()
    df.sort_values(by=['code_clean', 'date_dt'], inplace=True)

    for col in df.columns[-4:]:
        new_col_name = col + "_7day_rolling"
        new_column_names.append(new_col_name)
        df[new_col_name] = df[col]

    for code in df['code_clean'].unique():
        for col in new_column_names:
            df.loc[df['code_clean'] == code, col] = df.loc[df['code_clean'] == code, col].rolling(
                window=7).mean()

    df = df.rename(columns=clean_column_names)
    df = df.fillna(0)

    colsa = list(df.columns[0:4])
    colsb = list(df.columns[-4:])
    df_cols = colsa + colsb

    max_dt_raw = max(df['Date'])
    max_dt = max_dt_raw.strftime("%d_%m_%Y")

    #output_csv_file_path = './data/{}_{}.csv'.format(output_file_name, max_dt)
    output_pickle_file_path = './data/{}_{}.pkl'.format(output_file_name, max_dt)



    # df.to_csv("govt_data_daily_new_7dayAvg.csv", index=False)
    #df[df_cols].to_csv(output_csv_file_path, index=False)
    #df[df_cols].to_pickle(output_pickle_file_path)

    return df[df_cols]

##################################################################################
def clean_create_pivot(input_df, output_file_name):
    '''
    Helper function to clean the netherlands covid dataset.

    Params:
    ------
    df: pd.Dataframe
        Raw dataset as requested (in dataframe).

    Returns:
    -------
    pd.Dataframe
        Cleaned pivot table of data.

    '''
    new_column_names = []

    clean_column_names = {'code_clean': 'code',
                          'area_clean': 'name',
                          'date_dt': 'Date',
                          'cumCasesByPublishDate_ff': 'Cases (Cum)',
                          'cumCasesByPublishDateRate_ff': 'Case Rate (Cum)',
                          'cumPeopleVaccinatedFirstDoseByVaccinationDate_ff': 'Dose 1 (Cum)',
                          'cumPeopleVaccinatedSecondDoseByVaccinationDate_ff': 'Dose 2 (Cum)',
                          'cumPeopleVaccinatedThirdInjectionByVaccinationDate_ff': 'Dose 3 (Cum)'}

    df = input_df.copy()
    df['date_dt'] = pd.to_datetime(df["date"], format='%Y-%m-%d', errors='ignore')
    df['code_clean'] = df['areaCode'].apply(transform_code)
    df['area_clean'] = df['areaName'].apply(lambda x: transform_code(x, out='name'))
    df['country'] = df['code_clean'].astype(str).str[0]
    df = df[df['country'] == 'E']
    df = df.groupby(['code_clean', 'area_clean', 'date_dt']).sum()
    df = df.reset_index()
    df.sort_values(by=['code_clean', 'date_dt'], inplace=True)

    for col in df.columns[-5:]:
        new_col_name = col + "_ff"
        new_column_names.append(new_col_name)
        df[new_col_name] = df[col]

    for code in df['code_clean'].unique():
        for col in new_column_names:
            df.loc[df['code_clean'] == code, col] = df.loc[df['code_clean'] == code, col].replace(to_replace=0,
                                                                                                  method='ffill')
    df = df.rename(columns=clean_column_names)
    df['rate_divisor'] = df['name'].apply(rate_div)

    try:
        df['Case Rate (Cum) Backup'] = df['Case Rate (Cum)']
        df['Case Rate (Cum)'] = df['Case Rate (Cum)'].div(df['rate_divisor'])

        colsa = list(df.columns[0:3])
        colsb = list(df.columns[-6:-2])
        df_cols = colsa + colsb
    except:
        #colsa = list(df.columns[0:3])
        #colsb = list(df.columns[-6:-2])
        #df_cols = colsa + colsb
        df_cols = list(df.columns)

    df = df.fillna(0)

    # output_file_path = "./data/" + output_file_name + ".csv

    max_dt_raw = max(df['Date'])
    max_dt = max_dt_raw.strftime("%d_%m_%Y")

    output_csv_file_path = './data/{}_{}.csv'.format(output_file_name, max_dt)
    output_pickle_file_path = './data/{}_{}.pkl'.format(output_file_name, max_dt)

    #df[df_cols].to_csv(output_csv_file_path, index=False)
    #df[df_cols].to_pickle(output_pickle_file_path)

    return df[df_cols]
##############################


def merge_ltla_eng_data(url1, url2, function_name, description):
    '''
    Helper function to determine if the newest respective data file saved is from today.

    Params:
    ------
    url1 : str
        Permanent link provided from https://coronavirus.data.gov.uk/details/download to retrieve English Covid related data.

    url2 : str
        Permanent link provided from https://coronavirus.data.gov.uk/details/download to retrieve English Covid related data.

    function_name : function_name
        Name of function to retrieve the data
    
    description : str
        Description of data being pulled and merged.

    Returns:
    -------
    pd.dataframe
        Concatenated dataframes (LTLA + England Only Data)
    '''
    data_1 = return_gov_data(url1)
    df1 = function_name(data_1, description)

    data_2 = return_gov_data(url2)
    df2 = function_name(data_2, description)

    frames = [df1, df2]

    return pd.concat(frames)


#########################

def is_latest_file_from_today(fname_contains):
    '''
    Helper function to determine if the newest respective data file saved is from today.

    Params:
    ------
    fname_contains: str
        Partial filename to assist with filter and identification.

    Returns:
    -------
    URL: str
        Date in string format DD_MM_YYYY
    '''
    try:
        files = pd.DataFrame(glob.glob("./data/*.pkl"), columns=['path'])
        files['file_date'] = files['path'].apply(lambda x: x[-14:-4])
        max_d = max(files['file_date'].loc[files['path']
                                        .str.contains(fname_contains)])
        print(max_d) ###################################
        max_names = files['path'].loc[((files['file_date'] == max_d) &
                                       (files['path']
                                        .str.contains(fname_contains)))]
                                        #.str.contains('govt_data_daily_new')))]
        file_to_import = max_names.iloc[0]
        print(file_to_import)
        return file_to_import[-14:-4]
    except:
        print('except run')###################################
        return '01_01_2022'
