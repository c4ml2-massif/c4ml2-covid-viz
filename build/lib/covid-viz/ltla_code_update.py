def transform_code(item_in, out='code', code_dict={'E06000028': 'E06000058',
                                                   'E06000029': 'E06000058',
                                                   'E07000048': 'E06000058',
                                                   'E07000049': 'E06000059',
                                                   'E07000050': 'E06000059',
                                                   'E07000051': 'E06000059',
                                                   'E07000050': 'E06000059',
                                                   'E07000052': 'E06000059',
                                                   'E07000053': 'E06000059',
                                                   'E07000097': 'E07000242',
                                                   'E08000020': 'E08000037',
                                                   'E06000048': 'E06000057',
                                                   'E07000100': 'E07000240',
                                                   'E07000101': 'E07000243',
                                                   'E07000104': 'E07000241',
                                                   'E07000191': 'E07000246',
                                                   'E07000190': 'E07000246',
                                                   'E07000004': 'E06000060',
                                                   'E07000005': 'E06000060',
                                                   'E07000006': 'E06000060',
                                                   'E07000007': 'E06000060',
                                                   'E07000205': 'E07000244',
                                                   'E07000206': 'E07000244',
                                                   'E07000201': 'E07000245',
                                                   'E07000204': 'E07000245',
                                                   'E09000033': 'E41000324',
                                                   'E06000052': 'E41000052'},
                   name_dict={'Bournemouth':
                              'Bournemouth, Christchurch and Poole',
                              'Christchurch':
                              'Bournemouth, Christchurch and Poole',
                              'Poole':
                              'Bournemouth, Christchurch and Poole',
                              'East Dorset': 'Dorset',
                              'North Dorset': 'Dorset',
                              'Purbeck': 'Dorset',
                              'West Dorset': 'Dorset',
                              'Weymouth and Portland': 'Dorset',
                              'West Somerset': 'Somerset West and Taunton',
                              'Taunton Deane': 'Somerset West and Taunton',
                              'Aylesbury Vale': 'Buckinghamshire',
                              'Chiltern': 'Buckinghamshire',
                              'South Bucks': 'Buckinghamshire',
                              'Wycombe': 'Buckinghamshire',
                              'Suffolk Coastal': 'East Suffolk',
                              'Waveney': 'East Suffolk',
                              'Forest Heath': 'West Suffolk',
                              'St Edmundsbury': 'West Suffolk',
                              'Westminster': 'City of London,Westminster',
                              'Cornwall and Isles of Scilly':
                              'Cornwall,Isles of Scilly',
                              'Shepway': 'Folkestone and Hythe',
                              'Hackney and City of London': 'Hackney'}):
    '''
    This function takes either area code or area name from the geoJSON or
    gov.uk datasets for lower local tier authorities. If the code or name is
    not consistent across the geoJSON, gov.uk, and ONS source data, it is
    updated.

    Parameters
    ----------
    item_in :  string
        A single string containing either an area code or area name.
    out :  string, optional
        A string specifying whether a code (out='code') or area (out='area')
        update is needed. The default is 'code'
    code_dict :  dictionary, optional
        A dictionary that updates area codes which are not present in all
        three data sources. The default is {'E06000028': 'E06000058',
                                            'E06000029': 'E06000058',
                                            'E07000048': 'E06000058',
                                            'E07000049': 'E06000059',
                                            'E07000050': 'E06000059',
                                            'E07000051': 'E06000059',
                                            'E07000050': 'E06000059',
                                            'E07000052': 'E06000059',
                                            'E07000053': 'E06000059',
                                            'E07000097': 'E07000242',
                                            'E08000020': 'E08000037',
                                            'E06000048': 'E06000057',
                                            'E07000100': 'E07000240',
                                            'E07000101': 'E07000243',
                                            'E07000104': 'E07000241',
                                            'E07000191': 'E07000246',
                                            'E07000190': 'E07000246',
                                            'E07000004': 'E06000060',
                                            'E07000005': 'E06000060',
                                            'E07000006': 'E06000060',
                                            'E07000007': 'E06000060',
                                            'E07000205': 'E07000244',
                                            'E07000206': 'E07000244',
                                            'E07000201': 'E07000245',
                                            'E07000204': 'E07000245',
                                            'E09000033': 'E41000324',
                                            'E06000052': 'E41000052'}.
    name_dict :  dictionary, optional
        A dictionary that updates area names which are not present in all
        three data sources.
        The default is {'Bournemouth': 'Bournemouth, Christchurch and Poole',
                        'Christchurch': 'Bournemouth, Christchurch and Poole',
                        'Poole': 'Bournemouth, Christchurch and Poole',
                        'East Dorset': 'Dorset',
                        'North Dorset': 'Dorset',
                        'Purbeck': 'Dorset',
                        'West Dorset': 'Dorset',
                        'Weymouth and Portland': 'Dorset',
                        'West Somerset': 'Somerset West and Taunton',
                        'Taunton Deane': 'Somerset West and Taunton',
                        'Aylesbury Vale': 'Buckinghamshire',
                        'Chiltern': 'Buckinghamshire',
                        'South Bucks': 'Buckinghamshire',
                        'Wycombe': 'Buckinghamshire',
                        'Suffolk Coastal': 'East Suffolk',
                        'Waveney': 'East Suffolk',
                        'Forest Heath': 'West Suffolk',
                        'St Edmundsbury': 'West Suffolk',
                        'Westminster': 'City of London,Westminster',
                        'Cornwall and Isles of Scilly':
                        'Cornwall,Isles of Scilly',
                        'Shepway': 'Folkestone and Hythe',
                        'Hackney and City of London': 'Hackney'}}.

    Returns
    -------
    string

    '''
    # create a list of the codes that need to be updated
    old_codes = list(code_dict.keys())
    # determine if codes or names are being checked
    if out == 'code':
        if old_codes.count(item_in) == 1:
            # if the code is present in code_dict.keys() then it needs to be
            # updated
            new_code = code_dict[item_in]
        else:
            # else the input value is output
            new_code = item_in
        return(new_code)
    # determine if names are being checked instead of codes
    elif out == 'name':
        # create a list of the names that need to be updated
        names_to_update = list(name_dict.keys())
        if names_to_update.count(item_in) == 1:
            # the name is not consistent across all three data sources and so
            # is changed
            new_name = name_dict[item_in]
        else:
            # else simply return the original name
            new_name = item_in
        return(new_name)
    else:
        # dictionaries for only area codes and names have been set up with
        # this function
        return('Error: Input argument out must equal either "code" or "name"')
