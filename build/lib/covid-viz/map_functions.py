def map_outline(geo_df, choice_list, map_plot, map):
    
    '''
    Parameters
    ----------
    geo_df : Geopandas.DataFrame
        This is a dataframe that only includes the selected value for the most 
        recent date.
    choice_list : list
        List of the region user has selected in streamlit.
    map_plot : str
        A string of the first variable given in the select plot 
        input in streamlit.
    map : folium.Map
        This can either by the folium.Choropleth or the 
        folium.TimeSliderChoropleth map.

    Returns
    -------
    map : folium.Map
        This returns the map object with the ability to hover over regions,
        have tooltips popup with information on it, and highlight the first region. 
    '''

    from folium.features import GeoJson, GeoJsonTooltip, GeoJsonPopup
    import folium
    # Define the style and highlight hover functionality
    style_fun = lambda x: {'fillColor': '#ffffff',
                           'color': '#000000',
                           'fillOpacity': 0.1,
                           'weight': 0.1}
    highlight_fun = lambda x: {'fillColor': '#000000',
                               'color': '#000000',
                               'fillOpacity': 0.50,
                               'weight': 0.1}
    # Defines the popup
    popup = GeoJsonPopup(fields=['name', 'Median age (Rank)',
                                 'People/sq m (Rank)', 'Education % (Rank)',
                                 'Unemployed % (Rank)',
                                 'Key Workers % (Rank)',
                                 'Average Deprivation Score (Rank)',
                                 'White: Asian: Black: Mixed: Other (Rank)'],
                         aliases=['name', 'Median age (Rank)',
                                  'People/sq m (Rank)', 'Education % (Rank)',
                                  'Unemployed % (Rank)',
                                  'Key Workers % (Rank)',
                                  'Average Deprivation Score (Rank)',
                                  'White: Asian: Black: Mixed: Other (Rank)'],
                         localize=True,
                         labels=True,
                         sticky=True,
                         style="font-size: 9px",
                         max_width=150)

    # Defines the tooltip
    tooltip = GeoJsonTooltip(fields=['name'],
                             aliases=['name'],
                             localize=True,
                             labels=False,
                             style="""
                                        background-color: #F0EFEF;
                                        border: 1px solid black;
                                        border-radius: 3px;
                                        box-shadow: 3px;
                                        font-size: 10px;
                                    """,
                             max_width=150,
                             )

    # Creates a subset of the dataset to have a highlight function
    set_df = geo_df[geo_df["name"].isin(choice_list)]

    # Map to highlight and provide information on
    c_outline = folium.Choropleth(geo_data=set_df,
                                  name="Highlight",
                                  data=set_df,
                                  columns=["name", map_plot],
                                  key_on="feature.properties.code",
                                  fill_color="BuGn",
                                  fill_opacity=0,
                                  nan_fill_color="White",
                                  nan_fill_opacity=0,
                                  smooth_factor=0,
                                  line_color="Yellow",
                                  line_weight=2,
                                  line_opacity=1,
                                  reset=True,
                                  control=False,
                                  )

    # Removes the chloropleth bar that it generates
    for key in c_outline._children:
        if key.startswith('color_map'):
            del(c_outline._children[key])

    # Add outline to slider map
    c_outline.add_to(map)

    # Adds the tooltip and highlight function to the map
    folium.GeoJson(data=geo_df,
                   style_function=style_fun,
                   highlight_function=highlight_fun,
                   tooltip=tooltip,
                   popup=popup,
                   control=False).add_to(map)
    return map


def still_map(geo_df, choice_list, map_plot, zoom):
    '''

    Parameters
    ----------
    geo_df : Geopandas.DataFrame
        This is a dataframe that only includes the selected value for the most 
        recent date.
    choice_list : list
        List of the regions user has selected in streamlit.
    map_plot : str
        A string of the first variable given in the select plot 
        input in streamlit.
    zoom : Bool
        Boolean to identify whether the user wants to zoom in on this map.

    Returns
    -------
    m : folium.Choropleth map
        This returns a Choropleth map for the most recent values.

    '''
    import folium
    # If zoom boolean is checked then zooms in onto first region.
    if zoom:
        zoom_lat = geo_df["zoom_lat"][geo_df["name"] == choice_list[0]]
        zoom_lon = geo_df["zoom_lon"][geo_df["name"] == choice_list[0]]
        zoom_num = int(geo_df["zoom_num"][geo_df["name"] == choice_list[0]])
    # Else is set to England
    else:
        zoom_lat = 53.2
        zoom_lon = -1.5
        zoom_num = 6
    # Creates a folium figure
    f = folium.Figure(width=490, height=500)
    # This defines a folium map
    m = folium.Map(location=[zoom_lat, zoom_lon], tiles=None, maxZoom=12,
                   minZoom=4, zoom_start=[zoom_num],
                   maxBounds=[[60, -15], [45, 5]]).add_to(f)
    # This creates the Choropleth overlay
    c = folium.Choropleth(geo_data=geo_df,
                          name="Dataset 1",
                          data=geo_df,
                          columns=["name", map_plot],
                          key_on="feature.properties.name",
                          fill_color="PuBu",
                          nan_fill_color="White",
                          smooth_factor=0,
                          Highlight=True,
                          fill_opacity=0.7,
                          line_opacity=.1,
                          legend_name=map_plot,
                          overlay=True,
                          reset=True,
                          control=False
                          )
    c.add_to(m)
    # Return the map
    return m


def time_map(data, name_list, choice_list, map_plot):
    '''
    Parameters
    ----------
    data: Geopandas.DataFrame
        This is a dataframe that only includes the selected value for the most 
        recent date.
    name_list : list
        List of all possible regions.
    choice_list : list
        List of regions the user has selected in streamlit.
    map_plot : str
        A string of the first variable given in the select plot 
        input in streamlit.

    Returns
    -------
    m : folium.TimeSliderChoropleth map
        This returns a TimeSliderChoropleth map for the most recent values.
    '''

    import numpy as np
    import branca.colormap as cm
    from folium.plugins import TimeSliderChoropleth
    import geopandas as gpd
    import folium
    # Create the max and minimum colour
    max_colour = np.nanmax(data[map_plot])
    min_colour = np.nanmin(data[map_plot])
    # Defines the chloropleth bar
    cmap = cm.linear.BuPu_09.scale(min_colour, max_colour)
    data['colour'] = data[map_plot].map(cmap)
    # Creates the style dictionary
    style_dict = {}
    for i in range(0, len(name_list)):
        region = name_list[i]
        result = data[data['name'] == region]
        inner_dict = {}
        for _, r in result.iterrows():
            inner_dict[r['date_sec']] = {'color': r['colour'], 'opacity': 0.7,
                                         'smooth_factor': 0,
                                         'line_color': "#0000"}
        style_dict[str(i)] = inner_dict
    # Make a dataframe contaning the features for each country
    region_df = data[["geometry"]]
    region_gdf = gpd.GeoDataFrame(region_df)
    region_gdf = region_gdf.drop_duplicates().reset_index()
    # Creates the slider map
    slider_map = folium.Map(location=[52.7, -2.0], tiles=None,
                            zoom_start=6, maxBounds=[[60, -15], [45, 5]],
                            zoom_control=False)

    _ = TimeSliderChoropleth(data=region_gdf,
                             styledict=style_dict,
                             overlay=True,
                             name=map_plot,
                             control=False,
                             ).add_to(slider_map)
    # Adds the TimeSliderChoropleth map
    _ = cmap.add_to(slider_map)
    cmap.caption = map_plot
    # Return the TimeSliderChoropleth map
    return slider_map
