# COVID-Viz: COVID-19 Related Data Visualisation Tool for England

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Python 3.6+](https://img.shields.io/badge/python-3.6+-blue.svg)](https://www.python.org/downloads/release/python-360+/)

COVID-Viz has been developed to enable people to explore COVID-19 time-series data for lower tier local authorities in England.

 # Vision for COVID-Viz

 1. Deliver an all encompassing data visualisation tool to enable users to explore a range of relevant metrics related to COVID, including daily cases and vaccination rates.
 2. Allow users who may be unfamilar with a broad range of Python packages to access a simplistic, 'one-stop' data visualisation suite that is easy to use.
 3. Provide access to continually updated data (provided by [UK Government](https://coronavirus.data.gov.uk/))

# Features

1. Web-based data visualisation tool utisling [Streamlit](https://streamlit.io/).
2. Interactive cholorpleth maps linked with 2020 demographic data (provided by [Office for National Statistics](https://www.ons.gov.uk/peoplepopulationandcommunity/populationandmigration/populationestimates/articles/populationprofilesforlocalauthoritiesinengland/2020-12-14)). Boundaries of local authorities defined as per ONS Geoportal (provided by [Exploratory.io](https://exploratory.io/map)).
3. Dynamic time-series plots that allow  lower tier local authorities and COVID-19 metrics to be compared

## How to Explore This Pacakge

1. Clone this repository to your local machine and navigate into it (i.e. `cd c4ml2-covid-viz`)
2. Create a new environment from the file provided `conda env create -f environment.yml`
3. Activate the new environment `conda activate c4ml2-covid-viz`
4. Navidate to the 'covid-viz' subfolder (`cd covid-viz`)
5. Run the command `streamlit run main.py`
6. After running, a web-browser will launch with the main interface.
7. Refer to `user_guide.md` for instructions on how to use the tool.
