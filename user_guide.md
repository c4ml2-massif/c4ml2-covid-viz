# COVID-Viz User Guide
This package enables you, the user, to visualise a range of relevant metrics related to COVID-19 for each lower tier local authority in England, including daily cases and vaccination rates. This package is accessible for those who may be unfamiliar with python, or even those with an extensive background of python and the integrated packages. The package data, provided by the UK Government, is continually updated and used to create an easy to use 'one-stop' data visualisation suite. The visualation, both graphs and maps, released by the UK Governnment are segregated:

- The map and its corresponding graph are not displayed on a single page, and take some navigating to see
- Multiple metrics cannot be plotted together on a single plot
- The intervals defining the colour for each chloropleth tile are wide, resulting in a stagnant plot

The aim of this package is to build on these visualisations to create more integrated analysis. 

### Installing and Running the Package
The GitLab repository can be found following the link, where the package can be accessed: https://gitlab.com/c4ml2-massif/c4ml2-covid-viz/-/tree/main. Note that a GitLab account will be needed to access this. The installation process is simple and can be achieved in five simple steps:
1. Clone repository of package
2. Create new environment from the file provided 
3. Activate the new environment
4. Navigate to the 'covid-viz' subfolder
5. Run the command 'streamlit run main.py'
6. A web-browser will launch with the main interface
For more information about the installation, please see the README.md file within GitLab.

### Automatic Data Downloads
When the package app is first run a check is conducted to determine if the COVID-19 data stored within the package needs to be updated. If the most recent data to date matches the data stored within the package then the stored data will be used in the streamlit application, so as to avoid unneccessarily downloading the same data from the [UK Government](https://coronavirus.data.gov.uk/). If the latest data is incomplete i.e. it contains fewer datapoints that the stored default data; the dataset containing observations up to and including 11/01/2022, then the default dataset will be used.

### Using Streamlit
Once the window has opened, the screen should be split in two; a smaller column on the left with drop down functionalities, and a blank space on the right with the title "COVID-Viz". This space will be populated with a time-series graph and map of all lower tier local authorities once variables have been selected, as per image below:

<img src="./img/static-screenshot.png" alt="start up" width="900"/>

The choropleth map is composed of coloured polygons, each representing the local authorities, where a selected COVID-19 metric defines the colour. The darker the colour of a region, the larger the value of said metric; this makes it simple to identify local areas in which COVID-19 cases are higher. The map has two variations: the first uses the last 7 day average to colour the authorities, and the second displays data ranging from the most recent date to the last year, allowing the user to identify how the data has changed over this time period. To learn more about this type of chart, please follow this link: https://datavizcatalogue.com/methods/choropleth.html

Hovering over an authority with the cursor will display the authority's name. Clicking on said area will display key statistics reported by the Office of National Statistics (ONS); the statistics are the same as those used to order the regions, explained below, and consist of median age, people/sq m, education %, unemployed %, key workers %, average deprivation score, ethnicity ratio.

#### Customising the visualisation
Under the title *Customise Visuals*, the drop down list selects how the local authorities are ordered. The choice of socio-demographic variables consist of: 
- **Median age**: (1) has the highest median age - North Norfolk
- **People/ sq m**: (1) has the highest population by squared metre - Tower Hamlet
- **Education %**: (1) has the highest proportion of authority working in education - Oxford
- **Unemployed %**: (1) has the highest proportion of authority unemployed - Hartlepool
- **Key Workers %**: (1) has the highest proportion of authority working as key workers - Wyre
- **Average deprivation score**: (1) - Blackpool. Deprivation is measured as a combination of the following domains, and appropriately weighted when combined as per the percentages to obtain average deprivation score: income (22.5%), employment (22.5%), health deprivation and disability (13.5%), education and skills training (13.5%), crime (9.3%), barriers to housing and services (9.3%), living environment (9.3%). 
- **Ethinicity ratio**: (1) lowest percentage of white people (including travellers) - determining the most diverse lower tier local authority. Other ethnic groups include 'Asian/Asian British', 'Black/African/Caribbean/Black British', 'Other', which includes Arab, and 'Mixed/Multiple ethic group', taking multiple/mixed of all other ethnic groups. This metric inadvertently measures the most diverse authority - Newham

The *Customise Visuals* widget can be hidden by clicking the cross icon in its top right-hand corner and will reappear if the user clicks the arrow icon in the top up left-hand corner of the dashboard.

After selecting how you want the local authorities to be ranked, the app may appear to freeze but it is computing this calculation. This should only take a few moments. 

The local authority drop down list can select either a single, or multiple local authorities to visualise in the time series widget. Alternatively, the user can type the name of a particular authority in the drop down box instead of simple scrolling through the list provided. The choropleth map that displays only the last 7 day average has an extra functionality; this allows the user to zoom into the region selected. If multiple regions are selected, the zoom will be directed towards the first region selected. All selected local authorities are highlighted on the chloropleth map.

The **Select map type:** drop down list allows the user to switch between displaying the most recent data or the time series 1 week to 52 week time series, with the default map starting 4 weeks prior to the most recent data. The full time interval is determined through a slider in the left-hand widget, under the *Extra functionality:* box. After rerunning the app, a dynamic slider will appear in the dashboard, above the map itself. With this the user can update the map to show data from within any date of the specifed time interval. Note that the map will respond immediately to this slider hence there is no need to rerun the app with each change. Note that the further back the slider is set, the greater the rendering time for the map. The zoom functionality is not compatible with the time series map, therefore clicking this will not change the view.

The plot drop down list allows the user to select either a singular, or multiple variables to be plotted. Depending on the plot variable selected, the chart type will differ. For example if 'New Cases (7 Day Avg)' has been selected, both 7 day avg and daily cases will be plotted; the 7 day average line graph overlay's the daily cases histogram. 

An extra parameter is available; this scales selected variables between 0 and 1, allowing for clearer interpretation when multiple variables are selected and plotted. This is only compatible however, when one local authority is selected. For an example, the number of cases compared to number of vaccinations is small, therefore when these are plotted on the same graph, there is negligible change in the cases. 

*Run* triggers the app to refresh the visualisation view, the time series graph will appear first, and then the choropleth map. Please note this may take some time to run, so be patient. There won't be enough time however, to make a cuppa and grab a biscuit, instead enjoy the man in the right hand corner taking part in a multitude of sports. 

#### Examples of the dashboard display
<img src="./img/7-day_single_ltla.png" alt="7-day Avg, single authority" width="900"/>
<img src="./img/multiple_ltla.png" alt="multiple authorities " width="900"/>
<img src="./img/scaled_y_vars_daily.png" alt="multiple daily variables" width="900"/>
<img src="./img/scaled_y_vars_cumulative.png" alt="multiple cumulative variables" width="900"/>
<img src="./img/time_series_map.png" alt="multiple cumulative variables" width="900"/>


**This is an integrated visualation tool, therefore time to explore the potential may be required in order to experience the most benefit.**
