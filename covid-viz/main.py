'''
Main function that runs the streamlit app
'''

# Packages
import streamlit as st
import folium
import pandas as pd
import numpy as np
from streamlit_folium import folium_static
import geopandas as gpd
from folium.features import GeoJson, GeoJsonTooltip, GeoJsonPopup
from datetime import datetime
from datetime import timedelta
import branca.colormap as cm
from folium.plugins import TimeSliderChoropleth
import glob

# Importing functions from the package
import master_plotting as mp
import map_functions as mf
import data_clean as dc
import streamlit_input as si
import data_download_tools as ddt

# Setting the page_config layout to be wide
st.set_page_config(layout="wide")
# Setting the sidebar title
st.sidebar.title("Customise Visuals")

# Read in the GeoJSON datafile
geo_keep = gpd.read_file("data/LTA_Eng.GeoJSON")

# Dictionary associated to each daily_1 and pivot_2 dataframe
dp_dict = {'daily_1': {'df_name': 'daily_1',
                       'fname_desc': 'govt_data_daily_new_7dayAvg',
                       'fname_desc_backup':
                           './data/govt_data_daily_new_7day__Avg_CORE.pkl',
                       'num_rows': '197_000',
                       'merg_URL_1': ddt.NEW_DAILY_CASE_VAX_DATAURL,
                       'merg_URL_2': ddt.ENG_NEW_DAILY_CASE_VAX_DATAURL,
                       'merge_fn': ddt.clean_create_7day_rolling_average},
           'pivot_2': {'df_name': 'pivot_2', 'fname_desc': 'govt_data_pivot',
                       'fname_desc_backup':
                           './data/govt_data__pivot__CORE.pkl',
                       'num_rows': '180_000',
                       'merg_URL_1': ddt.CUM_DAILY_DATAURL,
                       'merg_URL_2': ddt.ENG_CUM_DAILY_DATAURL,
                       'merge_fn': ddt.clean_create_pivot}}

# Obtains todays date
now = datetime.now()
# Turns todays date into a string
today_str = now.strftime("%d_%m_%Y")
# Creates an allocation data dictionary
df_dict = {}

# For loop to check the location of the datafile using the dictionary for
# each dataframe
for dataset in dp_dict.keys():
    fname_desc = dp_dict.get(dataset).get('fname_desc')
    fname_desc_backup = dp_dict.get(dataset).get('fname_desc_backup')
    num_rows = dp_dict.get(dataset).get('num_rows')
    merg_URL_1 = dp_dict.get(dataset).get('merg_URL_1')
    merg_URL_2 = dp_dict.get(dataset).get('merg_URL_2')
    merge_fn = dp_dict.get(dataset).get('merge_fn')
    latest_file_date = ddt.is_latest_file_from_today(fname_desc)

    # Checks to see if the latest file exists
    try:
        # If it does it reads in the dataframe
        if now.strftime("%d_%m_%Y") == latest_file_date:
            file_name = './data/' + fname_desc + '_' 
            file_name = file_name + latest_file_date + '.pkl'
            df_dict[dataset] = pd.read_pickle(file_name)
        # If not the dataframe is loaded from the goverment website
        # And cleaned for use.
        else:
            print('error')
            df_dict[dataset] = ddt.merge_ltla_eng_data(merg_URL_1,
                                                       merg_URL_2,
                                                       merge_fn,
                                                       fname_desc)
            file_name = './data/' + fname_desc + '_' + today_str + '.pkl'

            df_dict[dataset].to_pickle(file_name)

        if df_dict[dataset].shape[0] < num_rows:
            df_dict[dataset] = pd.read_pickle(fname_desc_backup)
    # If the dataset cannot be updated the backup file is used
    # and updated to todays data for functionality issues.
    except:
        df_dict[dataset] = pd.read_pickle(fname_desc_backup)
        file_name = './data/' + fname_desc + '_' + today_str + '.pkl'
        df_dict[dataset].to_pickle(file_name)

# Read in the daily_1 dataframe
daily_1 = df_dict['daily_1']
# Read in the pivot_2 dataframe
pivot_2 = df_dict['pivot_2']

# List of options to sort the dataset by
rank_list = ['Median age', 'People/sq m', 'Education %',
             'Unemployed %', 'Key Workers %',
             'Average Deprivation Score', 'Ethnicity Ratio']
# Give an option to the user by what they want to rank by
rank_selected = st.sidebar.selectbox(
                                "Select how you want to sort your regions by:",
                                rank_list)

# Create a side bar for all the drop-down options
with st.sidebar.form("Configure"):
    # List of the two different map types
    map_type = ["Map with recent values",
                "Time Series Map"]
    # Select box input to get which map to plot
    map_selected = st.selectbox("Select map type:", map_type)
    # An optional zoom button that can be expanded for an input
    with st.expander("Extra functionality:"):
        # Adding a zoom checkbox functionality
        zoom = st.checkbox("Zoom in on Region")
        # Add the amount of weeks you want to go back by
        week_n = st.slider("Select the length of the time series",
                           1, 52, value=4)
    # Creates the three datasets needed for plotting
    covid_df, geo_df, geo_keep, time_choice = dc.data_curate(pivot_2,
                                                             daily_1,
                                                             geo_keep,
                                                             week_n)
    # Re-sort the dataframes to be ranked in order of selected option
    geo_df = dc.rank_sort(geo_df, rank_selected)
    geo_keep = dc.rank_sort(geo_keep, rank_selected)

    # Take England out of the two dataframes
    geo_df = geo_df[geo_df["name"] != 'England']
    geo_keep = geo_keep[geo_keep["name"] != 'England']

    # Obtain list of names in order of the dataframe sorted
    name_list = geo_keep["name"].tolist()
    # Obtain the ranks in order of the dataframe sorted
    rank_list = geo_keep[rank_selected]
    # List the rank list
    rank_list = list(rank_list)
    # Convert rankings to string
    rank_list = ['NA' if str(rank) == '<NA>' else str(rank)
                 for rank in rank_list]
    # Create a new name with it's rank for selecting
    name_rank = [f"{name_list[i]} ({rank_list[i]})"
                 for i in range(0, len(rank_list))]
    # Creating a dictionary for reactivity functionality
    name_dict = dict(zip(name_rank, name_list))
    # Add nanoseconds into the geo_df for plotting time series map
    geo_df["Date_2"] = geo_df["Date"] + timedelta(hours=12)
    geo_df['date_sec'] = geo_df["Date_2"].astype(np.int64) // 10**9
    geo_df['date_sec'] = geo_df['date_sec'].astype(int).astype(str)
    # Select the region of choice
    choice_selected = st.multiselect("Select region:", name_rank,
                                     default=name_rank[0])
    # Convert the choice selected to it's non-rank string version
    # and into a list
    choice_list = si.check_region_input(choice_selected, name_rank, name_dict)

    # List of time serial plotting choices
    time_choice = ['Case Rate (Cum)', 'Dose 1 (Cum)',
                   'Dose 2 (Cum)', 'Dose 3 (Cum)', "New Cases By Date",
                   'New Cases (7 Day Avg)', 'Dose 1 (7 Day Avg)',
                   'Dose 2 (7 Day Avg)', 'Dose 3 (7 Day Avg)']
    # Multiselect input to get the choices avalible to plot with
    plot_select = st.multiselect("Select plot:", time_choice,
                                 default='New Cases (7 Day Avg)')
    # Function to check plot_select inputs
    plot_select = si.check_yvar_input(plot_select)
    # Expander scaled button
    with st.expander("Extra parameter:"):
        scale = st.checkbox("Scale variables")
    # Redefine plot_select and zoom,and define map_plot
    plot_select, map_plot, scale = si.check_time_inputs(choice_list,
                                                        plot_select,
                                                        scale)
    # Make sure geo_keep is a geodataframe
    geo_keep = gpd.GeoDataFrame(geo_keep)

    # Creating a RUN button
    check = st.form_submit_button("Run")


# Create header section
header = st.container()
# Create column sections
col1, col2 = st.columns([1.7, 1])


if check:
    # With the Header plot the name of the app
    with header:
        st.title("COVID-Viz")

    # With colium one plot the time series plots
    with col1:
        # Use the ts_plot function to get the time series figure
        plot_fig, title_text = mp.ts_plot(regions=choice_list,
                                          y_var=plot_select,
                                          data=covid_df,
                                          scale=scale)
        # Set the title of the plot
        st.subheader(title_text)

        # Plots the time series figures and fixes it to the container
        st.plotly_chart(plot_fig, use_container_width=True)

    # With column 2 plot either Time series or recent value map
    # depending on the argument given.
    if map_selected == "Time Series Map":
        with col2:

            # Function that creates a time series choropleth
            slider_map = mf.time_map(geo_df, name_list, choice_list, map_plot)
            # A function to add highlighting, tooltip, and outline to the map.
            slider_map = mf.map_outline(geo_keep, choice_list, map_plot,
                                        slider_map)

            # Add light mode
            folium.TileLayer('cartodbpositron', name="light mode",
                             control=True).add_to(slider_map)

            # This plots the map in a static figure
            folium_static(slider_map, width=500, height=560)
    else:
        with col2:

            # Use the still_map function to generate the choropleth map
            recent_map = mf.still_map(geo_keep, choice_list, map_plot, zoom)

            # Use the map_outline function to add highlighting, tooltip,
            # and outline to the map.
            recent_map = mf.map_outline(geo_keep, choice_list, map_plot,
                                        recent_map)

            # Add light mode to the map
            folium.TileLayer('cartodbpositron', name="light mode",
                             control=True).add_to(recent_map)
            # Add dark mode to the map
            folium.TileLayer('cartodbdark_matter', name="dark mode",
                             control=True).add_to(recent_map)
            # Allow the user to control the layers for this map
            folium.LayerControl(collapsed=False).add_to(recent_map)

            # Then call the map using folium_static
            folium_static(recent_map, width=490, height=500)

    check = False
    zoom = False
    scale = False
