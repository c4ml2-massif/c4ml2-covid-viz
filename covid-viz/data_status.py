import data_download_tools as ddt
import pandas as pd
import glob
from datetime import datetime

def is_latest_file_from_today(fname_contains):
    try:
        files = pd.DataFrame(glob.glob("./data/*.pkl"), columns=['path'])
        files['file_date'] = files['path'].apply(lambda x: x[-14:-4])
        max_d = max(files['file_date'].loc[files['path']
                                        .str.contains(fname_contains)])
        max_names = files['path'].loc[((files['file_date'] == max_d) &
                                       (files['path']
                                        .str.contains(fname_contains)))]
                                        #.str.contains('govt_data_daily_new')))]
        file_to_import = max_names.iloc[0]
        return file_to_import[-14:-4]
    except:
        return '01_01_2022'

now = datetime.now()

latest_file_date = is_latest_file_from_today('govt_data_daily_new_7dayAvg')

if now.strftime("%d_%m_%Y") == latest_file_date:
    #print('same')
    ### no further action required
    file_name = './data/govt_data_daily_new_7dayAvg' + '_' + latest_file_date + '.pkl'
    #print(file_name)
    daily_1 = pd.read_pickle(file_name)
else:
    print('not')
    ##### download most recent
    try:
        daily_1 = ddt.merge_ltla_eng_data(ddt.new_daily_case_vax_dataURL,
                                         ddt.eng_new_daily_case_vax_dataURL,
                                         ddt.clean_create_7day_rolling_average,
                                         'govt_data_daily_new_7dayAvg')
    except:
        daily_1 = pd.read_pickle(ddt.locate_most_recent_local_data('govt_data_daily_new_7dayAvg'))

    if daily_1.shape[0]<197_000:
        daily_1 = pd.read_pickle(ddt.locate_next_to_most_recent_local_data('govt_data_daily_new_7dayAvg'))

now = datetime.now()

latest_file_date = is_latest_file_from_today('govt_data_pivot')

if now.strftime("%d_%m_%Y") == latest_file_date:
    #print('same')
    ### no further action required
    file_name = './data/govt_data_pivot' + '_' + latest_file_date + '.pkl'
    #print(file_name)
    pivot_2 = pd.read_pickle(file_name)
else:
    print('not')
    ##### download most recent
    try:
        pivot_2 = ddt.merge_ltla_eng_data(ddt.cum_daily_dataURL,
                                     ddt.eng_cum_daily_dataURL,
                                     ddt.clean_create_pivot,
                                     'govt_data_pivot')
    except:
        pivot_2 = pd.read_pickle(ddt.locate_most_recent_local_data('govt_data_pivot'))

    if daily_1.shape[0]<197_000:
        pivot_2 = pd.read_pickle(ddt.locate_next_to_most_recent_local_data('govt_data_pivot'))