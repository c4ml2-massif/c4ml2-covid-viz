def check_region_input(choice_selected, name_rank, name_dict):
    '''
    Parameters
    ----------
    choice_selected : list
        This is a list of the regions the user has chosen.
    name_rank : list
        This is a list of the regions with a concatenation of 
        the rank chosen for displaying to the user.
    name_dict : dict
        This is a dictionary which converts the region concatenated
        with the rank string back to the original region string.

    Returns
    -------
    choice_list : list
        Returns the converted regions chosen back to their original
        string input.

    '''
    # Checks if the user has removed all defaults
    if len(choice_selected) == 0:
        # Converts region string without ranks
        choice = name_dict[name_rank[0]]
        # Creates choice list
        choice_list = [choice]
    # Checks if the user only had one input
    elif type(choice_selected) == str:
        # Converts region string without ranks
        choice = name_dict[choice_selected]
        # Creates choice list
        choice_list = list(choice)
    # The rest should be a list of inputs
    else:
        # Creates choice list using a for loop
        choice_list = []
        for choice in choice_selected:
            # Converts region string without ranks
            choice = name_dict[choice]
            # Appends to choice_list
            choice_list.append(choice)
    # Returns choice_list
    return choice_list


def check_yvar_input(plot_select):
    '''
    Parameters
    ----------
    plot_select : str/list
        This is a string/list of the variables the user can select
        to plot with.

    Returns
    -------
    plot_select : list
        This converts the string to a list if it needs it.

    '''
    # Check if the user has removed the default
    if len(plot_select) == 0:
        # If not New cases (7 day avg) is plotted
        plot_select = ['New Cases (7 Day Avg)']
    # If not check if the input is only a string
    elif type(plot_select) == str:
        # Then convert plot_select to a list with
        # one variable
        plot_select = list(plot_select)
    # Else the input should be a list
    else:
        # Create an allocation list
        plot_list = []
        # Create a for loop to obtain the y-variables
        for plot_val in plot_select:
            # Append each y_variable to the plot list
            plot_list.append(plot_val)
        # Then redefines plot_select
        plot_select = plot_list
    # Returns the plot_select list
    return plot_select


def check_time_inputs(choice_list, plot_select, scale):
    '''
    Parameters
    ----------
    choice_list : list
        This is a list of the regions the user has selected.
    plot_select : str/list
        This is a string or list of the variables the user
        has selected to plot with.
    scale : Bool
        This is a Boolean of whether the user wants to scale
        the data.

    Returns
    -------
    list
        A list containing: plot select, a list of the options the user has used;
        map_plot is the first option selected in plot select; if scale cannot be
        altered for the plot selected it is changed back to a false. 

    '''
    # Check if the choice_list and plot_select have many variables
    if len(choice_list) > 1 and len(plot_select) > 1:
        # Redefine plot_select to only plot the first input
        plot_select = plot_select[0]
        # Define map_plot as plot_select
        map_plot = plot_select
        # Remove the ability to scale the data
        scale = False
    # If the plot_select list has many variables
    elif len(choice_list) == 1 and len(plot_select) > 1:
        plot_select = plot_select
        # map_plot is defined as the first plot_select option
        map_plot = plot_select[0]
        # Zoom is left to have functionality
        scale = scale
    else:
        plot_select = plot_select
        # map plot is defined as the first plot_select
        map_plot = plot_select[0]
        # Remove the ability to scale the data
        scale = False

    # Return plot_select, map_plot and zoom
    return [plot_select, map_plot, scale]
