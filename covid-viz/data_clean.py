def data_curate(pivot_2, daily_1, geo_keep, week_n):

    '''
    Parameters
    ----------
    pivot_2 : pd.DataFrame
        This is a dataframe containing the cumulative COVID
        data from the goverments website.
    daily_1 : pd.DataFrame
        This is a dataframe containing the daily COVID data from
        the goverments website.
    geo_keep : gpd.GeoDataFrame
        This is a dataframe containing the geometry polygons 
        merged with the ONS sociodemographic data. 
    week_n : int
        This is an integer that the user will select to determine
        how far back they can go on the time series.

    Returns
    -------
    list
        Returns a list contianing: covid_df, a merged dataframe
        of both pivot_2 and daily_1; geo_df containing a merged
        dataframe of covid_df and geo_keep and filtered by the
        time series data needed; geo_keep which is then redefined
        with the most up-to-date time serial data; time_choice which
        is a list of all COVID data column options.

    '''
    import pandas as pd
    #from datetime import datetime
    #from datetime import timedelta

    # Merge the dataframe for time series plotting
    covid_df = pd.merge(pivot_2, daily_1)
    # Change to date
    covid_df["Date"] = pd.to_datetime(covid_df["Date"]).tolist()
    # Obtaining plot_select options
    time_choice = covid_df.columns.tolist()
    #time_choice = time_choice[3:]
    time_choice[7] = 'New Cases By Date'
    covid_df.columns = time_choice
    time_choice = time_choice[3:]

    # Create geo_df for map time series
    # Get's today date
    max_date = max(covid_df["Date"].unique())
    week_date = max_date - pd.Timedelta(7*week_n, unit='D')
    # Filters the dataset to be above this date
    time_df = covid_df[covid_df["Date"] > week_date]
    # Creates the new geo_df
    geo_df = pd.merge(time_df, geo_keep)
    # Redefine the Geo_Keep dataframe
    # Extracts the max date from the geo_df
    max_date = max(geo_df["Date"].unique())
    # Only keep the max date from the covid dataframe
    geo_keep = geo_df[geo_df["Date"] == max_date]
    # Reset the index
    geo_keep = geo_keep.reset_index(drop = True)
    # Remove Date from the dataframe
    old_col = geo_keep.columns.tolist()
    new_col = [col_name for col_name in old_col if col_name != 'Date']
    geo_keep = geo_keep[new_col]
    return [covid_df, geo_df, geo_keep, time_choice]


def rank_sort(data, rank_selected):

    '''
    Parameters
    ----------
    data : pd.DataFrame
        Reads in a dataframe then sort by the sociodemographic
        rank specified and extracts the ranks.         
    rank_selected : str
        This is a string of the sociodemgraphic data given from
        the user.

    Returns
    -------
    data : pd.DataFrame
        Returns the new sorted dataframe.

    '''

    import pandas as pd
    # Convert rank integer for sorting
    for i in range(0, 6):
        data[rank_selected] = data[rank_selected].astype('Int64')
    # Extract the England dataset
    eng_data = data[data["name"] == 'England']
    rank_data = data[data["name"] != 'England']
    # Sorting vaue by the rank selected
    rank_data = rank_data.sort_values(by = rank_selected,
                                      axis = 0, ascending = True)
    # Put England back in
    data = pd.concat([eng_data, rank_data], axis = 0)
    data = data.reset_index(drop = True)
    return data