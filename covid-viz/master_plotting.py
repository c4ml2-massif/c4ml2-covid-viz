def ts_plot(regions, y_var, data, scale=False):
    '''
    Generate a time series plot for daily COVID-19 cases and vaccinations,
    for either a single lower tier local authority, or multiple.

    Parameters
    ----------
    regions : str or list
        Either a string of a single local tier authority's name, or a list of
        strings containing the names of multiple specified areas.
    y_var : str or list
        Either a string of a single COVID-19 metric such as 'New Cases (7 Day
        Avg)', or a list of strings referring to multiple COVID-19 metrics.
    data : pd.DataFrame
        A government dataset containing time-series data for a specified set
        of lower tier local authorities and a specified set of COVID-19
        metrics.
    scale : bool, optional
        When True this scales all y-variables plotted to make comparison
        between different COVID-19 metric easier. The default is False.

    Returns
    -------
    [plotly.graph_objs._figure.Figure, str].

    '''
    # import packages
    import pandas as pd
    import numpy as np
    import plotly.express as px
    ##########################################################################
    # check for the number of regions provided
    # if only a string is provided (i.e. a single area name) convert it into a
    # list
    if type(regions) == str:
        regions = [regions]
    # if only a string is provided (i.e. a single metric) convert it into a
    # list
    if type(y_var) == str:
        y_var = [y_var]
    # plotting 7_day average and daily cases for a single area
    # for this metric only a combination of bars and lines will be used
    if (len(y_var) == 1 and y_var[0] == 'New Cases (7 Day Avg)' and
        len(regions) == 1):
        # plot the 7-day average then add on the daily cases with a different
        # histogram
        title_text = f'Time Series of {y_var[0]} in {regions[0]}'
        # first plot the bars of the daily cases
        # create a y-label plain English
        fig = px.bar(data[data['name'] == regions[0]], x='Date',
                     y='New Cases By Date',
                     labels = {'New Cases By Date': y_var[0]})
        # assign a colour to the bars
        fig.update_traces(marker_color='black')
        # subset data to the single area provided
        df = data[data['name'] == regions[0]]
        # plot the 7 day average line
        fig.add_scatter(x=df['Date'], y = df[y_var[0]],
                             mode ='lines', name = '7 day average',
                line = dict(color = "red"))
        # set x-label and disable plotly zoom function
        fig.update_xaxes(title_text = 'Date', fixedrange=True)
        # add in the range selector and dynamic buttons
        fig.update_xaxes(rangeselector=dict(
            buttons=list([
                dict(count=1, label='1m', step='month', stepmode='backward'),
                dict(count=6, label='6m', step='month', stepmode='backward'),
                dict(count=1, label='YTD', step='year', stepmode='todate'),
                dict(count=1, label='1y', step='year', stepmode='backward'),
                dict(step='all')
            ])), rangeslider_visible=True)
    # Plot multiple y-variables with one region
    elif len(y_var) > 1 and len(regions) == 1:
        # subset on singular region
        df = data[data['name'] == regions[0]]
        if scale is True:
            for i in np.arange(0, len(y_var)):
                df[y_var[i]] = (df[y_var[i]] - min(df[y_var[i]])) / (max(df[y_var[i]]) - min(df[y_var[i]]))
        # concatenate each metric to use in the title string
        y_var_text = ', '.join(y_var)
        title_text = f'Time Series of {y_var_text} in {regions[0]}'
        # initalise line
        fig = px.line(df, x="Date", y=y_var[0])
        # initialise the legend to describe each line
        fig.update_layout(showlegend=True)
        # iterate through each metric and add the corresponding line
        for i in np.arange(0, len(y_var)):
            fig.add_scatter(x=df["Date"], y=df[y_var[i]],
                            mode='lines', name = y_var[i])
        # set x-label and disable plotly zoom function
        fig.update_xaxes(title_text = 'Date', fixedrange=True)
        if scale is True:
            fig.update_yaxes(title_text = 'Variables Scaled to max')
        # add in the range selector and dynamic buttons
        fig.update_xaxes(rangeselector=dict(
            buttons=list([
                dict(count=1, label='1m', step='month', stepmode='backward'),
                dict(count=6, label='6m', step='month', stepmode='backward'),
                dict(count=1, label='YTD', step='year', stepmode='todate'),
                dict(count=1, label='1y', step='year', stepmode='backward'),
                dict(step='all')
            ])), rangeslider_visible=True)
    # line plot for one region and one metric only
    elif len(regions) == 1:
        # generate title text
        y_var_text = ', '.join(y_var)
        title_text = f'Time Series of {y_var_text} in {regions[0]}'
        # remember to add in labels if need be using labels = dict()
        fig = px.line(data[data['name'] == regions[0]], x='Date', y=y_var[0])
        # set x-label and disable plotly zoom function
        fig.update_xaxes(title_text = 'Date', fixedrange=True)
        # add dynamic range slider and buttons
        fig.update_xaxes(rangeselector=dict(
            buttons=list([
                dict(count=1, label='1m', step='month', stepmode='backward'),
                dict(count=6, label='6m', step='month', stepmode='backward'),
                dict(count=1, label='YTD', step='year', stepmode='todate'),
                dict(count=1, label='1y', step='year', stepmode='backward'),
                dict(step='all')
            ])), rangeslider_visible=True)
    # multiple regions scenario:
    else:
        # initiliase the line plot with the first region given
        fig = px.line(data[data['name'] == regions[0]], x='Date', y=y_var[0])
        # initialise the legend to describe each line
        fig.update_layout(showlegend=True)
        # iterate through each region and plot each line of the single metric
        for i in np.arange(0, len(regions)):
            # subset on a specific area
            df = data[data['name'] == regions[i]]
            # add one line per area
            fig.add_scatter(x = df['Date'],
                            y = df[y_var[0]],
                            mode ='lines',
                            name = regions[i])
        # generate title text
        title_text = f'Time Series of {y_var[0]}'
        fig.update_xaxes(fixedrange=True)
        # add in range slider and buttons
        fig.update_xaxes(rangeselector=dict(
            buttons=list([
                dict(count=1, label="1m", step="month", stepmode="backward"),
                dict(count=6, label="6m", step="month", stepmode="backward"),
                dict(count=1, label="YTD", step="year", stepmode="todate"),
                dict(count=1, label="1y", step="year", stepmode="backward"),
                dict(step="all")
            ])), rangeslider_visible=True)
    # output the figure and its corresponding title
    return(fig, title_text)
