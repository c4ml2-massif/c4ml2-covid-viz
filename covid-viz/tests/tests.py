import pytest
from c4ml2-covid-viz.covid-viz.data_download_tools import return_gov_data
import pandas as pd
import numpy as np
import csv
import datetime
from seleniumbase import BaseCase
import cv2
import time

def dataset_columns():
    '''
    Test the number of columns from the government data matches the expected
    '''
    # expected columns from government data download
    expected_columns = ['areaCode', 'areaName', 'areaType', 'date',
                        'cumCasesByPublishDate','cumCasesByPublishDateRate',
                        'cumPeopleVaccinatedFirstDoseByVaccinationDate',
                        'cumPeopleVaccinatedSecondDoseByVaccinationDate',
                        'cumPeopleVaccinatedThirdInjectionByVaccinationDate']
    # URL to download data
    URL = 'https://api.coronavirus.data.gov.uk/v2/data?areaType=ltla&' \
    'metric=cumCasesByPublishDate&metric=cumPeopleVaccinatedFirstDoseBy' \
    'VaccinationDate&metric=cumPeopleVaccinatedSecondDoseByVaccinationDate' \
    '&metric=cumPeopleVaccinatedThirdInjectionByVaccinationDate&metric=' \
    'cumCasesByPublishDateRate&format=csv'
    # check number of columns is the same, none have been added or removed
    len_cols = return_gov_data(URL).shape[1]
    assert len_cols == len(expected_columns)
    # check name of columns are the same, no change in variable names
    columns = pd.Index.tolist(return_gov_data(URL).columns)
    assert columns == expected_columns
    
def dataset_unique_districts():
    '''
    Test the number of lower tier local authorities outputted from the
    government data matches expected. 
    '''
    # number of lower tier local authorities from government data
    expected_districts = 380
    # URL to download ata
    URL = 'https://api.coronavirus.data.gov.uk/v2/data?areaType=ltla&' \
    'metric=cumCasesByPublishDate&metric=cumPeopleVaccinatedFirstDoseBy' \
    'VaccinationDate&metric=cumPeopleVaccinatedSecondDoseByVaccinationDate' \
    '&metric=cumPeopleVaccinatedThirdInjectionByVaccinationDate&metric=' \
    'cumCasesByPublishDateRate&format=csv'
    # obtain the number of unique districts from government download
    districts = len(return_gov_data(URL)['areaName'].unique())
    assert districts == expected_districts

def test_basic():
    '''
    Test the Document Object Model (DOM) structure remains the same as saved
    "static-screenshot.png" NB// values not necessarily need to be equal
    '''
    # open the app and take a screenshot
    open("http://localhost:8501")
    time.sleep(10)  # give leaflet time to load from web
    save_screenshot("..\img\current-screenshot.png")
    # test screenshots look exactly the same
    original = cv2.imread(
        "..\img\static-screenshot.png")
    duplicate = cv2.imread("..\img\current-screenshot.png")
    assert original.shape == duplicate.shape
    difference = cv2.subtract(original, duplicate)
    b, g, r = cv2.split(difference)
    assert cv2.countNonZero(b) == cv2.countNonZero(g) == cv2.countNonZero(r) == 0
    
def dataset_ltla_check(data):
    '''
    Test that input dataset contains the same unique 312 lower tier local
    authority area name-code pairs as reference_codes.csv.
    '''
    # assuming a code column of name 'code' and name column of name 'name'
    # read in reference codes - update this to import data from within the
    # package
    reference_codes = pd.read_csv('reference_codes.csv')
    # create a single column for the code-name pairs in the input dataset
    data['pair_id'] = data['name'] + ' ' + data['code']
    gov_pair_id = pd.Series(data['pair_id'].sort_values().drop_duplicates())
    # compare the length of each set of code-name pairs
    assert len(gov_pair_id) == len(reference_codes['pair_id'])
