import pathlib
from setuptools import setup

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

# This call to setup() does all the work
setup(
    name="covid-viz",
    version="0.1.0",
    author="Coding4ML_Massif",
    author_email="ec722@exeter.ac.uk",    
    description="Package for Interactive Maps and Charts related to English COVID-19 Cases",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/c4ml2-massif/c4ml2-covid-viz",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Operating System :: OS Independent"
    ],
    py_modules=["covid-viz"], 
    packages=["covid-viz"], 
    include_package_data=True,
    install_requires=["streamlit>=1.3.1", "folium>=0.11.0", "streamlit-folium>=0.4.0", "pandas", "plotly", "plotly-express"],

)
